import Vue from 'vue';
import App from './App.vue';
import store from './store';
import { SvgIconPlugin } from './plugins/svg-icon';

Vue.config.productionTip = false;

Vue.use(SvgIconPlugin);

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
