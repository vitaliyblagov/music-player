import { Track } from '@/typings/player';

export type RootState = {
  list: Track[]
  activeIndex: number
  isPlaying: boolean
  filter: string
};
