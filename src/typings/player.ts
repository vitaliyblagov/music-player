export type Track = {
  title: string
  author: string
  media: string
  link: string
  duration: string
};
