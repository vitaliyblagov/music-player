import { AudioPlayer, PlayerEvents } from '@/services/AudioPlayer';
import mitt from 'mitt';

const emitter = mitt<PlayerEvents>();

export const player = new AudioPlayer(emitter);
