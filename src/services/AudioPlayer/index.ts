import { Emitter } from 'mitt';

const DEFAULT_VOLUME = 0.75;

export type PlayerEvents = {
  timeupdate: number
  loadedmetadata: undefined
  play: undefined
  pause: undefined
  resume: undefined
  ended: undefined
}

export class AudioPlayer {
  private readonly element: HTMLAudioElement;
  private readonly emitter: Emitter<PlayerEvents>;
  volume = DEFAULT_VOLUME;
  src?: string;
  isPlaying = false;
  currentTime = 0;
  duration = 0;

  constructor(emitter: Emitter<PlayerEvents>) {
    this.element = new Audio();
    this.emitter = emitter;

    this.setupListeners();
    this.setVolume(DEFAULT_VOLUME);
  }

  private setupListeners() {
    this.element.addEventListener('timeupdate', this.onTimeUpdate.bind(this));
    this.element.addEventListener('loadedmetadata', this.onLoadedMetadata.bind(this));
    this.element.addEventListener('ended', this.onEnded.bind(this));
    this.element.addEventListener('play', this.onPlay.bind(this));
    this.element.addEventListener('pause', this.onPause.bind(this));
  }

  private onLoadedMetadata() {
    this.duration = this.element.duration;
    this.emitter.emit('loadedmetadata');
  }

  private onTimeUpdate() {
    this.setCurrentTime(this.element.currentTime);
    this.emitEvent('timeupdate', this.element.currentTime);
  }

  private onEnded() {
    this.emitEvent('ended');
  }

  private onPlay() {
    this.isPlaying = true;
    this.emitEvent('play');
  }

  private onPause() {
    this.isPlaying = false;
    this.emitEvent('pause');
  }

  private setAudioSrc(src: string): void {
    this.src = src;
    this.element.src = src;
    this.isPlaying = false;
  }

  private setCurrentTime(progress: number): void {
    this.currentTime = progress;
  }

  private emitEvent<T extends keyof PlayerEvents>(event: T, arg?: PlayerEvents[T]) {
    this.emitter.emit(event, arg as never);
  }

  private validateVolume(volume: number): number {
    if (volume < 0) return 0;
    if (volume > 1) return 1;

    return volume;
  }

  on<T extends keyof PlayerEvents>(event: T, callback: (arg: PlayerEvents[T]) => void): void {
    this.emitter.on(event, callback);
  }

  off<T extends keyof PlayerEvents>(event: T, callback: (arg: PlayerEvents[T]) => void): void {
    this.emitter.off(event, callback);
  }

  play(src?: string): Promise<void> {
    const isNewTrack = src !== this.src;

    if (!src && !this.src) throw new Error('You have to set up src first');
    if (isNewTrack) this.setAudioSrc(src as string);

    return this.element.play();
  }

  pause(): void {
    this.element.pause();
  }

  setVolume(volume: number): void {
    this.volume = volume;
    this.element.volume = this.validateVolume(volume);
  }

  mute(): void {
    this.element.muted = true;
  }

  unmute(): void {
    this.element.muted = false;
  }

  setProgress(progress: number): void {
    this.element.currentTime = progress;
  }
}
