export function formatSeconds(secs: number): string {
  const minutes = String(Math.floor(secs / 60)).padStart(2, '0');
  const seconds = Number(secs % 60).toFixed().padStart(2, '0');

  return `${minutes}:${seconds}`;
}
