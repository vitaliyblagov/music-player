import { PluginObject } from 'vue';

import SvgIcon from './component.vue';

export const SvgIconPlugin: PluginObject<void> = {
  install(Vue) {
    const req = require.context('@/assets/icons', false, /\.svg$/);
    const requireAll = (requireContext: __WebpackModuleApi.RequireContext) => requireContext.keys().map(requireContext);
    requireAll(req);

    Vue.component('svg-icon', SvgIcon);
  }
};
