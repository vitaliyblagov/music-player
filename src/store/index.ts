import Vue from 'vue';
import Vuex from 'vuex';
import { Track } from '@/typings/player';
import { player } from '@/services/AudioPlayer/plugin';
import { RootState } from '@/typings/store';

Vue.use(Vuex);

export default new Vuex.Store<RootState>({
  state: () => ({
    list: [],
    activeIndex: 0,
    isPlaying: false,
    filter: ''
  }),
  mutations: {
    SET_LIST(state, list: Track[]) {
      state.list = list;
    },
    SET_ACTIVE_INDEX(state, index: number) {
      state.activeIndex = index;
    },
    SET_IS_PLAYING(state, isPlaying: boolean) {
      state.isPlaying = isPlaying;
    },
    SET_FILTER(state, query: string) {
      state.filter = query;
    }
  },
  getters: {
    activeTrack(state): Track {
      return state.list[state.activeIndex];
    },
    filteredList(state): Track[] {
      const { list, filter } = state;
      const query = filter.toLowerCase();

      if (!filter) return list;
      return list
        .filter(({ author, title }) => author.toLowerCase().includes(query) || title.toLowerCase().includes(query));
    }
  },
  actions: {
    playTrack({ getters, commit }, index: number) {
      commit('SET_ACTIVE_INDEX', index);

      const { media } = getters.activeTrack;
      if (player.isPlaying && media === player.src) {
        player.pause();
      } else {
        player.play(media);
      }
    },
    next({ state, dispatch }) {
      const { activeIndex, list } = state;
      const nextTrack = activeIndex + 1 === list.length ? 0 : activeIndex + 1;

      dispatch('playTrack', nextTrack);
    },
    prev({ state, dispatch }) {
      const { activeIndex, list } = state;
      const nextTrack = activeIndex - 1 < 0 ? list.length - 1 : activeIndex - 1;

      dispatch('playTrack', nextTrack);
    },
    search({ commit }, query: string) {
      commit('SET_FILTER', query);
    }
  }
});
