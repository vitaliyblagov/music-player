module.exports = {
  chainWebpack: config => {
    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();

    svgRule
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .tap(() => ({ symbolId: 'icon-[name]' }));
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
        @import "@/assets/style/vars.scss";
        @import "@/assets/style/global.scss";
        `
      }
    }
  }
};
